from awaymo.settings import *


SECRET_KEY = '%25xgdj5828o*e5$!l2$%fxr)#*nm=qqmkink3xuvq096biy&e'
DEBUG = True

ALLOWED_HOSTS = ['*']

"""
Uncomment this if want to change database settings in development mode


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_DB', 'awaymo'),
        'USER': os.environ.get('POSTGRES_USER', 'awaymo_user'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD', 'awaymo_password'),
        'HOST': os.environ.get('POSTGRES_HOST', 'localhost'),
        'PORT': os.environ.get('POSTGRES_PORT', 5430),
    }
}
"""