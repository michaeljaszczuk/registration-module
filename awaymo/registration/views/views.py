
from rest_framework.decorators import action
from rest_framework.response import Response

from registration import registration
from registration.views.viewsets import BaseStepView
from registration.views.mixins import StepCreateModelMixin, StepUpdateModelMixin, StepRetrieveModelMixin


class RegistrationStepView(StepCreateModelMixin,
                           StepUpdateModelMixin,
                           StepRetrieveModelMixin,
                           BaseStepView):

    def get_serializer_class(self):
        if self.action == 'create' or self.action == 'retrieve':
            step = 0
        else:
            step = registration.get_object_step_number(self.kwargs[self.lookup_field])

        step_object = registration.get_step(step)
        return step_object.get_serializer()

    @action(detail=True, methods=['post'])
    def cancel(self, request, pk=None):
        instance = self.get_object()
        if registration.is_finished(instance) or registration.is_pending(instance):
            step = registration.get_object_step(instance)
            return Response(step.get_serializer_data(instance), status=405)

        registration.stop_current_step(instance)
        return Response(status=204)