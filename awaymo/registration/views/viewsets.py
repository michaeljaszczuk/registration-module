from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework.viewsets import ViewSet

from registration import registration


class BaseStepView(ViewSet):
    lookup_field = 'pk'

    def get_object(self):
        try:
            instance = registration.get_object(self.kwargs[self.lookup_field])
        except ObjectDoesNotExist as ex:
            raise Http404(ex)
        self.check_object_permissions(self.request, instance)
        return instance

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.

        You may want to override this if you need to provide different
        serializations depending on the incoming request.

        (Eg. admins get full serialization, others get basic serialization)
        """
        assert self.serializer_class is not None, (
                "'%s' should either include a `serializer_class` attribute, "
                "or override the `get_serializer_class()` method."
                % self.__class__.__name__
        )

        return self.serializer_class

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }
