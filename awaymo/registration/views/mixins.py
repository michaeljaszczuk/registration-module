from time import sleep

from django.http import Http404
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin, RetrieveModelMixin
from rest_framework.response import Response

from registration import registration
from registration.utils import StepCancelledException


class StepRetrieveModelMixin(RetrieveModelMixin):
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        step = registration.get_object_step(instance)
        serializer_data = step.get_serializer_data(instance)
        return Response(serializer_data)


class StepCreateModelMixin(CreateModelMixin):
    def perform_create(self, serializer):
        instance = serializer.save()
        registration.finish_current_step(instance)
        current_step_number = registration.next_step(instance)
        registration.run_next_step(instance, current_step_number)


class StepUpdateModelMixin(UpdateModelMixin):
    def perform_update(self, serializer):
        instance = serializer.save()
        registration.finish_current_step(instance.pk)
        current_step_number = registration.next_step(instance.pk)
        registration.run_next_step(instance.pk, current_step_number)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        step = registration.get_object_step(instance)
        if (
                registration.is_stopped(instance.pk) or
                registration.is_finished(instance.pk) or
                registration.is_pending(instance.pk)
        ):
            return Response(step.get_serializer_data(instance), status=405)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
