DEFAULTS = {
    'MANAGER': {
        'CLASS': 'DjangoModelManager',
        'ATTR': {
            "model": "registration.User"
        }
    },

    'STEPS': [
        {
            'CLASS': 'create_user',
            'ATTR': {
                'view_based_step': True,
            }
        },
        {
            'CLASS': 'details_user',
            'ATTR': {
                'view_based_step': True,
            }
        },
        {
            'CLASS': 'api_check_user',
            'ATTR': {
                'view_based_step': False,
            }
        },
    ],
}
