from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'email', 'password', 'registration_step', 'registration_step_status')
        read_only_fields = ('pk', 'registration_step', 'registration_step_status')
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user_data = {'is_active': False}
        user_data.update(**validated_data)

        user = User.objects.create_user(**user_data)
        return user

    def save(self, **kwargs):
        if self.instance:
            self.instance.refresh_from_db()
        return super().save(**kwargs)


class DetailsUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'email', 'first_name', 'last_name')
        read_only_fields = ('pk', 'email')
        extra_kwargs = {
            'first_name': {'allow_null': False, 'required': True},
            'last_name': {'allow_null': False, 'required': True}
        }

    def save(self, **kwargs):
        if self.instance:
            self.instance.refresh_from_db()
        return super().save(**kwargs)


class LastConfirmSerializer(serializers.HyperlinkedModelSerializer):
    confirm = serializers.BooleanField(required=True)

    class Meta:
        model = User
        fields = ('confirm',)

    def save(self, **kwargs):
        if self.instance:
            self.instance.refresh_from_db()
        instance = super().save(**kwargs)
        instance.is_active = self.validated_data['confirm']
        instance.save()
        return instance
