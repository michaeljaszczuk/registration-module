from registration import conf
from registration.managers.object_managers import get_manager
from registration.managers.step_managers import StepManager


class RegistrationModule:
    def __init__(self, configuration):
        steps_configuration = configuration.get('STEPS')
        self.step_manager = StepManager(steps_configuration)
        self.last_step_index = len(steps_configuration) - 1
        self.object_manager = None

    def __get_manager(self):
        if self.object_manager is None:
            self.object_manager = get_manager()
        return self.object_manager

    def get_step(self, id):
        """
        Method to get step by id.
        :param id: id of step
        :return: step object
        """
        return self.step_manager.step(id)

    def get_object_step_number(self, data):
        """
        Method to get currently processed object instance step id
        :param data: processed object instance lookup
        :return: step id
        """
        return self.__get_manager().get_current_step_number(data)

    def get_object_step(self, data):
        """
        Method to get currently processed object instance step
        :param data: processed object instance lookup
        :return: step object
        """
        return self.get_step(self.__get_manager().get_current_step_number(data))

    def get_object(self, data):
        """
        Mthod to get object instance.
        :param data: processed object instance lookup
        :return: object instance
        """
        return self.__get_manager().get(data)

    def finish_current_step(self, instance):
        """
        Method to manage object instance and set current step as finished.
        :param instance: object instance
        :return: step id
        """
        return self.__get_manager().finish_current_step(instance, self.last_step_index)

    def stop_current_step(self, instance):
        """
        Method to manage object instance and set current step as stopped.
        :param instance: object instance
        :return: step id
        """
        return self.__get_manager().stop_current_step(instance)

    def start_current_step(self, instance):
        """
        Method to manage object instance and set current step as pending.
        :param instance: object instance
        :return: step id
        """
        return self.__get_manager().start_current_step(instance)

    def next_step(self, instance):
        """
        Method to manage object instance and set object current step to next one.
        :param instance: object instance
        :return: step id
        """
        return self.__get_manager().next_step(instance, self.last_step_index)

    def run_next_step(self, instance, current_step):
        """
        Method to run not view based steps
        :param instance: object instance
        :param current_step: id of step
        :return: None
        """
        step_object = self.get_step(current_step)

        if not self.__get_manager().is_status_finished(instance):
            # Run next step if next step is not view based step
            if step_object and not step_object.view_based_step:
                step_object.run(instance)

    def is_finished(self, instance):
        """
        Method to check if object is already finished.
        :param instance: object instance
        :return: True/False
        """
        current_step = self.__get_manager().get_current_step_number(instance)
        return current_step == self.last_step_index and self.__get_manager().is_status_finished(instance)

    def is_stopped(self, instance):
        """
        Method to check if object is already stopped.
        :param instance: object instance
        :return: True/False
        """
        return self.__get_manager().is_status_stopped(instance)

    def is_pending(self, instance):
        """
        Method to check if object is already finished.
        :param instance: object instance
        :return: True/False
        """
        current_step = self.__get_manager().get_current_step_number(instance)
        return current_step < self.last_step_index and self.__get_manager().is_status_pending(instance)

    def get_object_data(self, data, field_name):
        """
        Method to get data from object instance by field name.
        :param data: object instance
        :param field_name: name of looking field
        :return: object
        """
        return self.__get_manager().get_object_data(data, field_name)

    def update_object_data(self, data, kwargs):
        """
        Method to update object instance
        :param data: object instance
        :param kwargs:
        :return:
        """
        return self.__get_manager().update_object_data(data, kwargs)


registration = RegistrationModule(conf.DEFAULTS)
