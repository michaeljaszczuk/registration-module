from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _

from registration.utils import DjangoChoices


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class StatusChoices(DjangoChoices):
    PENDING = (1, 'pending')
    DONE = (2, 'done')
    STOPPED = (4, 'stopped')
    NEW = (5, 'new')


class User(AbstractUser):
    """User model."""

    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    registration_step = models.PositiveSmallIntegerField(default=0)
    registration_step_status = models.PositiveSmallIntegerField(
        default=StatusChoices.NEW.value[0],
        choices=StatusChoices.choices()
    )
    registration_step_error = models.TextField(null=True, blank=True)

    # External API data
    accepted = models.BooleanField(null=True)
    name = models.CharField(max_length=150, null=True, blank=True)

    objects = UserManager()

    def __set_status(self, status, commit=True):
        self.registration_step_status = status

        if commit:
            self.save()

    def update_external_api_data(self, commit=True, **kwargs):
        self.accepted = kwargs['accepted']
        self.name = kwargs['name']
        if commit:
            self.save()

    def __is_status(self, status):
        return self.registration_step_status == status

    def is_status_finished(self):
        return self.__is_status(StatusChoices.DONE.value[0])

    def is_status_stopped(self):
        return self.__is_status(StatusChoices.STOPPED.value[0])

    def is_status_pending(self):
        return self.__is_status(StatusChoices.PENDING.value[0])

    def get_current_step_number(self):
        return self.registration_step

    def start_current_step(self, commit=True):
        new_status = StatusChoices.PENDING.value[0]
        self.__set_status(new_status, commit)
        return self.registration_step

    def stop_current_step(self, commit=True):
        new_status = StatusChoices.STOPPED.value[0]
        self.__set_status(new_status, commit)
        return self.registration_step

    def finish_current_step(self, commit=True):
        new_status = StatusChoices.DONE.value[0]
        self.__set_status(new_status, commit)
        return self.registration_step

    def next_step(self, last_step_index=1, commit=True):
        current_step = self.registration_step
        if current_step < last_step_index:
            self.registration_step = current_step + 1
            self.registration_step_status = StatusChoices.NEW.value[0]
            if commit:
                self.save()
        return self.registration_step
