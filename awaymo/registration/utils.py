from enum import Enum


class StepObjectNotFoundError(Exception):
    pass


class RegistrationStepNotFound(Exception):
    pass


class ObjectDoesNotExist(Exception):
    pass


class AwaymoApiError(Exception):
    pass


class StepCancelledException(Exception):
    pass


class DjangoChoices(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.value[0], x.value[1]) for x in cls)
