from awaymo.celery import app
from registration import get_manager, registration
from registration.connectors.awaymo_api_connector import AwaymoApiConnector
from registration.utils import AwaymoApiError


@app.task(bind=True, autoretry_for=(Exception,), default_retry_delay=5, retry_kwargs={'max_retries': 5})
def api_check_user_task(self, data):


    # Set current step as pending
    registration.start_current_step(data)

    # Get instance once
    instance = registration.get_object(data)
    check_api_connector = AwaymoApiConnector()
    response = check_api_connector.check_user_data(
        registration.get_object_data(instance, 'first_name'),
        registration.get_object_data(instance, 'last_name')
    )

    if not response.ok:
        raise AwaymoApiError(response.json())

    registration.update_object_data(data, response.json())
    registration.finish_current_step(data)

    current_step_number = registration.next_step(data)
    registration.run_next_step(data, current_step_number)
