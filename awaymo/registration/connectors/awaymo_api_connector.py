import requests

from awaymo import settings


class AwaymoApiConnector:
    def __init__(self):
        self.__check_api_url = settings.AWAYMO_API_URL

    def check_user_data(self, first_name, last_name):
        data = {
            "first_name": first_name,
            "last_name": last_name,
        }
        return requests.post(url=self.__check_api_url, json=data)
