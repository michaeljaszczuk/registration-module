from django.conf.urls import url, include
from registration import views
from registration.routers import CustomStepRouter

custom_router = CustomStepRouter()
custom_router.register(r'registration', views.RegistrationStepView)

urlpatterns = [
    url(r'^', include(custom_router.urls)),
]
