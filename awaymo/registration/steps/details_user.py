from registration.serializers import DetailsUserSerializer
from registration.steps.step import BaseStep, BaseViewStepMixin


class DetailsUserStep(BaseViewStepMixin, BaseStep):
    name = "details_user"
    serializer = DetailsUserSerializer