import abc

from registration import registration
from registration.managers.object_managers import get_manager


class BaseStep:
    __metaclass__ = abc.ABCMeta

    name = None
    objects = None

    def __init__(self, view_based_step=True):
        self.view_based_step = view_based_step
        self.objects = get_manager()

        assert self.name is not None, (
                "'%s' should include a `name` attribute"
                % self.__class__.__name__
        )

    @abc.abstractmethod
    def get_serializer(self):
        pass

    @abc.abstractmethod
    def get_object(self, lookup, *args, **kwargs):
        pass

    @abc.abstractmethod
    def post_save(self):
        pass

    @abc.abstractmethod
    def get_current_step(self, **kwargs):
        pass

    @abc.abstractmethod
    def get_status_response(self, instance):
        pass



class BaseViewStepMixin:
    def __init__(self, view_based_step=True):
        super().__init__(view_based_step)

        assert self.serializer is not None, (
                "'%s' should include a `serializer` attribute"
                % self.__class__.__name__
        )

    def get_serializer(self):
        return self.serializer

    def get_object(self, lookup, *args, **kwargs):
        return self.objects.get(lookup)

    def get_current_step(self, lookup):
        return self.objects.get_step(lookup)

    def get_status_response(self, instance):
        raise NotImplementedError

    def post_save(self):
        raise NotImplementedError

    def get_serializer_data(self, instance):
        step = instance.registration_step
        if instance.is_status_finished() and step == registration.last_step_index:
            details_message = 'Registration process finished.'
        else:
            steps_left = registration.last_step_index - step + 1
            details_message = f'There is {steps_left} more registration steps.'
        return {
            'status': instance.get_registration_step_status_display(),
            'step': instance.registration_step,
            'details': details_message
        }


class BaseNotViewStepMixin:

    def get_serializer_data(self, instance):
        step = instance.registration_step_status
        if instance.is_status_finished() and step == registration.last_step_index:
            details_message = 'Registration process finished.'
        else:
            details_message = f'There is {registration.last_step_index-step} more registration steps.'
        return {
            'status': instance.get_registration_step_status_display(),
            'step': instance.registration_step,
            'details': details_message
        }

    def get_serializer(self):
        raise NotImplementedError

    def get_object(self, lookup, *args, **kwargs):
        raise NotImplementedError

    def post_save(self):
        raise NotImplementedError

    def get_current_step(self, lookup):
        raise NotImplementedError

    def get_object(self, lookup, *args, **kwargs):
        raise NotImplementedError

    def get_current_step(self, lookup):
        raise NotImplementedError


def get_step(step_name=""):
    step_name = step_name.lower()

    for step_cls in BaseStep.__subclasses__():
        if step_cls.name == step_name:
            return step_cls

    raise TypeError(f'Step class `{step_name}` not found. Check documentation how to add new Step class.')
