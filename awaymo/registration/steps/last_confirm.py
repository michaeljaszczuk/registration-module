from registration.serializers import LastConfirmSerializer
from registration.steps.step import BaseStep, BaseViewStepMixin


class LastConfirmUserStep(BaseViewStepMixin, BaseStep):
    name = "last_confirm"
    serializer = LastConfirmSerializer