from registration.serializers import UserSerializer
from registration.steps.step import BaseStep, BaseViewStepMixin


class CreateUserStep(BaseViewStepMixin, BaseStep):
    name = "create_user"
    serializer = UserSerializer