from registration.steps.step import BaseStep, BaseNotViewStepMixin
from registration.tasks import api_check_user_task


class ApiCheckUserStep(BaseNotViewStepMixin, BaseStep):
    name = "api_check_user"

    def run(self, instance):
        api_check_user_task.delay(instance)
