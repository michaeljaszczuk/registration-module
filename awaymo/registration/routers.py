from rest_framework.routers import SimpleRouter, Route, DynamicRoute


class CustomStepRouter(SimpleRouter):
    """
    A router for read-only APIs, which doesn't use trailing slashes.
    """

    def get_default_basename(self, viewset):
        return "registration"
