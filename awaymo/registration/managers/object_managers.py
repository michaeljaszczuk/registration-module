import abc

from django.apps import apps as django_apps

from registration.conf import DEFAULTS
from registration.utils import StepCancelledException


class BaseObjectManager:
    """
    Class to manage all data operations.
    """
    __metaclass__ = abc.ABCMeta

    object_manager = None

    @abc.abstractmethod
    def get(self, data):
        """
        Method to get one object instance based by data.
        :param data: Information about object to get
        :return: Object
        """
        pass

    @abc.abstractmethod
    def all(self):
        """
        Method to get all objects.
        :return: Iterable object
        """
        pass

    @abc.abstractmethod
    def get_current_step_number(self, data):
        """
        Method to get object current step instance based by data.
        :param data: Information about object to get
        :return: Boolean, Object
        """
        pass

    @abc.abstractmethod
    def finish_current_step(self, data, last_step_index):
        """
        Method to manage object instance and set current step as finished.
        :param data: object instance
        :param last_step_index: Last step index
        :return: step id
        """
        pass

    @abc.abstractmethod
    def next_step(self, data, last_step_index):
        """
        Method to manage object instance and set object current step to next one.
        :param data: object instance
        :param last_step_index: Last step index
        :return: step id
        """
        pass

    @abc.abstractmethod
    def is_status_finished(self, data):
        """
        Method to get if current step is finished
        :param data: object instance
        :return: True/False
        """
        pass

    @abc.abstractmethod
    def is_status_pending(self, data):
        """
        Method to get if current step is pending
        :param data: object instance
        :return: True/False
        """
        pass

    @abc.abstractmethod
    def stop_current_step(self, data):
        """
        Method to manage object instance and set object current step as stopped.
        :param data: object instance
        :return: step id
        """
        pass

    @abc.abstractmethod
    def start_current_step(self, data):
        """
        Method to manage object instance and set object current step as pending.
        :param data: object instance
        :return: step id
        """
        pass

    @abc.abstractmethod
    def get_object_data(self, data, field_name):
        """
        Method to get data from object instance by field name.
        :param data: object instance
        :param field_name: name of looking field
        :return: object
        """
        pass

    @abc.abstractmethod
    def update_object_data(self, data, kwargs):
        """
        Method to update data objecin t instance .
        :param data: object instance
        :param kwargs:
        :return: object
        """
        pass


class DjangoModelManager(BaseObjectManager):
    """
    Access to Django models as step objects.
    """

    model = None
    lookup_field = ''

    def __init__(self, *args, **kwargs):
        self.model = kwargs.get('model', None)
        self.lookup_field = kwargs.get('lookup_field', 'pk')

        assert self.model is not None, (
                "'%s' should include a `model` attribute"
                % self.__class__.__name__
        )

        self.model = django_apps.get_model(self.model, require_ready=False)

    def get(self, data):
        if isinstance(data, self.model):
            return data
        # try:
        return self.model.objects.get(**{self.lookup_field: data})
        # except self.model.DoesNotExist as ex:
        #     raise ObjectDoesNotExist(ex)

    def all(self):
        return self.model.objects.all()

    def get_current_step_number(self, data):
        assert getattr(self.model, 'get_current_step_number', None) is not None, (
                "'%s' should include a `get_current_step_number` method"
                % self.model
        )
        if isinstance(data, self.model):
            obj = data
        else:
            obj = self.get(data)
        return obj.get_current_step_number()

    def finish_current_step(self, data, last_step_index):
        instance = self.get(data)
        self.__check_if_cancelled(instance)
        return instance.finish_current_step(last_step_index)

    def next_step(self, data, last_step_index):
        instance = self.get(data)
        self.__check_if_cancelled(instance)
        return instance.next_step(last_step_index)

    def is_status_finished(self, data):
        instance = self.get(data)
        return instance.is_status_finished()

    def is_status_pending(self, data):
        instance = self.get(data)
        return instance.is_status_pending()

    def is_status_stopped(self, data):
        instance = self.get(data)
        return instance.is_status_stopped()

    def stop_current_step(self, data):
        instance = self.get(data)
        return instance.stop_current_step()

    def start_current_step(self, data):
        instance = self.get(data)
        self.__check_if_cancelled(instance)
        return instance.start_current_step()

    def get_object_data(self, data, field_name):
        instance = self.get(data)
        return getattr(instance, field_name)

    def update_object_data(self, data, kwargs):
        instance = self.model.objects.filter(**{self.lookup_field: data.pk if isinstance(data, self.model) else data})
        self.__check_if_cancelled(instance[0])
        return instance.update(**kwargs)

    def __check_if_cancelled(self, instance):
        if instance.is_status_stopped():
            raise StepCancelledException()


def get_manager():
    manager_name = DEFAULTS['MANAGER']['CLASS']
    for step_cls in BaseObjectManager.__subclasses__():
        if step_cls.__name__ == manager_name:
            return step_cls(**DEFAULTS['MANAGER']['ATTR'])

    raise TypeError(
        f'ObjectManager class `{manager_name}` not found. Check documentation how to add new ObjectManager class.')
