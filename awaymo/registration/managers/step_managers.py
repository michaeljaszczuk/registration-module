from registration.conf import DEFAULTS
from registration.utils import RegistrationStepNotFound


class StepManager:
    def __init__(self, *args, **kwargs):
        self.STEPS = DEFAULTS['STEPS']

    def step(self, id):
        from registration.steps.step import get_step
        try:
            step_cls = get_step(self.STEPS[id]['CLASS'])
        except IndexError:
            return None
        return step_cls(**self.STEPS[id].get('ATTR', None))


