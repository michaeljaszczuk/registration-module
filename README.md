# registration-module

Application to manage registration process in specific steps.

## Getting Started [using Docker]

### Requirements
- [Docker](https://docs.docker.com/install/) - Containers-as-a-Service (CaaS) platform used in project
- [docker-compose](https://docs.docker.com/compose/install/) - Tool for defining and running multi-container
### Run application [Develop mode]
Build and run development mode in docker containers
```Bash
docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml up -d
```
Default:
Application is running now on: localhost:8092
Database (PostgreSQL) access: localhost:5430

### Run application [Production mode]
Build and run production mode in docker containers.

1. Create config.env file using template
```Bash
cp config_template.env config.env
```
2. Update config.env file

* SECRET_KEY - Django settings SECRET_KEY value
* DEBUG - Django settings DEBUG value
* ALLOWED_HOSTS - Django settings SECRET_KEY value
* POSTGRES_DB - Django settings DATABASES NAME value
* POSTGRES_USER - Django settings DATABASES USER value
* POSTGRES_PASSWORD - Django settings DATABASES PASSWORD value
* POSTGRES_PORT - Django settings DATABASES PORT value
* POSTGRES_HOST - Django settings DATABASES HOST value

3. Run application
```Bash
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
```

Default:
Application is running now on: localhost:8092

## Getting Started [using local environment]
### Requirements
* [Python3+](https://www.python.org/) - Programming language used in project
* [pip](https://pypi.org/project/pip/) - Tool for installing Python packages.
### Run application
1. Install requirements from requirements.txt
```Bash
pip install -r requirements.txt
```
2. Add database settings in settings_dev.py
3. Run application
```Bash
python manage.py runserver --settings=awaymo.settings_dev
```

## Running the tests
Coming soon...

# Documentation

## conf.py
```
'MANAGER': {
        'CLASS': 'DjangoModelManager',
        'ATTR': {
            "model": "registration.User"
        }
},

'STEPS': [
    {
        'CLASS': 'create_user',
        'ATTR': {
            'view_based_step': True,
        }
    },
    {
        'CLASS': 'details_user',
        'ATTR': {
            'view_based_step': True,
        }
    },
    {
        'CLASS': 'api_check_user',
        'ATTR': {
            'view_based_step': False,
        }
    },
],
```
* MANAGER - Object manager using by steps and view
* STEPS - 'CLASS' - name of step class
* STEPS - 'ATTR' - list of attributes

## object manager
* BaseObjectManager - Abstract class to manage all object operations
* DjangoModelManager - Manager operating on Django ORM

## steps
* BaseStep - Abstract step class
* BaseViewStepMixin - Step mixin requiring update by api request
* BaseNotViewStepMixin - Step mixin not requiring update by api request and runing automatically after previous step

## adding new step
* Create step object inheriting BaseStep and one of Mixins

```
class ExampleStep(BaseViewStepMixin, BaseStep):
    name = "example"
    serializer = ExampleSerializer

class ExampleAutoStep(BaseNotViewStepMixin, BaseStep):
    name = "example_auto"

    def run():
        print("DONE")
```
* import new step in steps/__init__.py
```
from .my_steps import ExampleStep, ExampleAutoStep
```
* add step to conf.py file
```
'STEPS': [
        {
            'CLASS': 'example',
            'ATTR': {
                'view_based_step': True,
            }
        },
        {
            'CLASS': 'example_auto',
            'ATTR': {
                'view_based_step': False,
            }
        },
]
```
## API
* [POST] api/v1/registration/ - run first step
* [PUT] api/v1/registration/{pk}/ - run next step
* [GET] api/v1/registration/{pk}/ - get current status
* [POST] api/v1/registration/{pk}/cancel - cancel current step

# About
Worklog info in worklog.txt
Created by Michał Jaszczuk

